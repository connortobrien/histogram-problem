package main

import (
	"fmt"
	"math"
)

func findMinHeight(hist []int) int {
	minHeight := math.Inf(1)
	for _, val := range hist {
		minHeight = math.Min(float64(val), minHeight)
	}
	return int(minHeight)
}

func splitByHeight(hist []int, height int) [][]int {
	splits := make([][]int, 0)
	splits = append(splits, make([]int, 0))
	count := 0
	for i, val := range hist {
		if val <= height {
			count++
			if i != len(hist) {
				splits = append(splits, make([]int, 0))
			}
		} else {
			splits[count] = append(splits[count], val)
		}
	}
	return splits
}

// func maxRectangularArea(hist []int) int {
// 	area := 0.0
// 	return int(area)
// }

func main() {
	histA := []int{1, 3, 2, 0, 4, 7, 0, 2, 1}
	fmt.Println(splitByHeight(histA, 1))
}
